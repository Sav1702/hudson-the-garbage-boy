using System;
using System.Drawing;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace Hudson
{
    public enum Direction
    {
        Right = 1,
        Left = 2,
        Up = 3,
        Down = 4
    }

    public class HudsonSprite : PictureBox
    {
        public int Lives { get; set; }

        public int Inventory { get; set; }

        public int FormHeight { get; set; }

        public int FormWidth { get; set; }

        public int Speed { get; set; }

        public void Move(Direction direction)
        {
            if (direction == Direction.Up)
                Top -= Speed;
            else if (direction == Direction.Down)
                Top += Speed;
            else if (direction == Direction.Left)
                Left -= Speed;
            else if (direction == Direction.Right)
                Left += Speed;

            Top = Math.Min(Math.Max(Top, Height / 5), FormHeight - Height * 2);
            Left = Math.Min(Math.Max(Left, Width / 5), FormWidth - Width * 2);
        }
    }

    public class Truck : PictureBox
    {
        public Point Track;

        public Point track1L;
        public Point track1R;
        public Point track2L;
        public Point track2R;
        public int Speed { get; set; }

        public Direction CurrentDirection { get; set; }
        public int OffsetX { get; set; }

        public void setTrackLocation(Point trak1L, Point trak1R, Point trak2L, Point trak2R)
        {
            track1L = trak1L;
            track1R = trak1R;
            track2L = trak2L;
            track2R = trak2R;
        }

        public void collisionAvoidance(PictureBox trk1L, PictureBox trk1R, PictureBox trk2L, PictureBox trk2R)
        {
            if (CurrentDirection == Direction.Right)
            {
                if (Bounds.IntersectsWith(trk1R.Bounds) || Bounds.IntersectsWith(trk2R.Bounds))
                {
                    OffsetX = -100;
                    Reset();
                }
            }
            else if (CurrentDirection == Direction.Left)
            {
                if (Bounds.IntersectsWith(trk1L.Bounds) || Bounds.IntersectsWith(trk2L.Bounds))
                {
                    OffsetX = 100;
                    Reset();
                }
            }
        }

        public void Drive()
        {
            if (CurrentDirection == Direction.Left)
                Location = new Point(Location.X - Speed, Location.Y);
            else if (CurrentDirection == Direction.Right)
                Location = new Point(Location.X + Speed, Location.Y);
        }

        public void Reset()
        {
            Location = new Point(Track.X + OffsetX, Track.Y);
        }
    }

    public class TrashCan : PictureBox
    {
        public int inventory { get; set; }
        public int inventoryMax { get; set; }
    }

    public class GameScore
    {
        public int Score = 0;
    }

    


}