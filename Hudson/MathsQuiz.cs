﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace Hudson
{
    public partial class MathsQuiz : Form
    {
        int ans;
        int count = 0;
        int id;
        GameScore globalscore;
        public MathsQuiz(GameScore globalscore, int id)
        {
            this.id = id;
            this.globalscore = globalscore;
            InitializeComponent();
            generateQ();
        }

        public void generateQ()
        {
            Random randint = new Random();
            int operand1 = randint.Next(1, 11);
            int operand2 = randint.Next(1, 11);

            int qType = randint.Next(1, 5);
            if(qType == 1)
            {
                ans = operand1 + operand2;
                lblQuestion.Text = operand1 + "+" + operand2;
            }
            else if(qType == 2)
            {
                ans = operand1 - operand2;
                lblQuestion.Text = operand1 + "-" + operand2;
            }
            else if(qType == 3)
            {
                ans = operand1 * operand2;
                lblQuestion.Text = operand1 + "*" + operand2;
            }
            else if(qType == 4)
            {
                ans = operand1 / operand2;
                lblQuestion.Text = operand1 + "/" + operand2;
            }
        }

        private void checkAnswer(int answer)
        {
            try
            {
                if (Convert.ToInt32(txtAnswer.Text) == answer)
                {
                    correct();
                }
                else
                {
                    incorrect();
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Please enter a number");
            }
        }

        private void correct()
        {
            MessageBox.Show("Correct!");
            globalscore.Score += 10;
            lblScore.Text = "Score: " + globalscore.Score;
            count++;
            txtAnswer.Clear();
            if (count != 3)
                generateQ();
            else
            {
                FinishGame();
                Close();
            }
        }

        private void incorrect()
        {
            MessageBox.Show("Incorrect!");
            txtAnswer.Clear();
            count++;
            if (count != 3)
                generateQ();
            else
            {
                FinishGame();
                Close();
            }
        }

        private void btnAnswer_Click(object sender, EventArgs e)
        {
            checkAnswer(ans);
        }


        public void FinishGame()
        {
            var dbConnection = new SQLiteConnection("Data Source=Database.db;Version=3;");
            var db = new Database(dbConnection);

            var score = new Score
            {
                userID = id,
                score = globalscore.Score,
                date = DateTime.Today.ToString()
            };

            db.scores.InsertOnSubmit(score);
            try
            {
                db.SubmitChanges();
            }
            catch (SQLiteException exception)
            {
                db.SubmitChanges();
            }
        }

    }
}
