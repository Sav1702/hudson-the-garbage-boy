﻿using System;
using System.Windows.Forms;

namespace Hudson
{
    public partial class landing : Form
    {
        private int id;
        public landing(int id)
        {
            this.id = id;
            InitializeComponent();
        }


        private void btnPlay_Click(object sender, EventArgs e)
        {
            this.Hide();
            Game game = new Game(id);
            game.Show();
            game.Closed += (s, args) => Show();
        }

        private void BtnScores_Click(object sender, EventArgs e)
        {
            this.Hide();
            Scores scoreswindow = new Scores(id);
            scoreswindow.Show();
            scoreswindow.Closed += (s, args) => Show();
        }
    }
}
