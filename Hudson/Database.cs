﻿using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data.SQLite;

namespace Hudson

{    
    [Table(Name = "Users")]
    public class user
    {
        [Column(IsPrimaryKey = true)]
        public int? id { get; set; }

        [Column]
        public string username { get; set; }

        [Column]
        public string password { get; set; }

    }

    [Table(Name = "Scores")]
    public class Score
    {
        [Column(IsPrimaryKey = true)] public int? id { get; set; }

        [Column] public int userID { get; set; }

        [Column] public string date { get; set; }

        [Column] public int score { get; set; }

    }


    [Database]
    public partial class Database  : DataContext
    {
        public Table<user> users;
        public Table<Score> scores;


        public Database(SQLiteConnection connection) : base(connection) { }
    }
}
