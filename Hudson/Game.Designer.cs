using System.ComponentModel;

namespace Hudson
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pbRoad1 = new System.Windows.Forms.PictureBox();
            this.pbRoad2 = new System.Windows.Forms.PictureBox();
            this.gameTimer = new System.Windows.Forms.Timer(this.components);
            this.lblLives = new System.Windows.Forms.Label();
            this.pbLitter1 = new System.Windows.Forms.PictureBox();
            this.pbLitter2 = new System.Windows.Forms.PictureBox();
            this.pbLitter3 = new System.Windows.Forms.PictureBox();
            this.pbLitter4 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.track1R = new System.Windows.Forms.PictureBox();
            this.track1L = new System.Windows.Forms.PictureBox();
            this.track2L = new System.Windows.Forms.PictureBox();
            this.track2R = new System.Windows.Forms.PictureBox();
            this.lblInv = new System.Windows.Forms.Label();
            this.pbtrashCan = new Hudson.TrashCan();
            this.truck45 = new Hudson.Truck();
            this.truck2 = new Hudson.Truck();
            this.truck0 = new Hudson.Truck();
            this.hudsonSprite1 = new Hudson.HudsonSprite();
            this.truck1right = new Hudson.Truck();
            this.truck1R = new Hudson.Truck();
            this.truck1 = new Hudson.Truck();
            ((System.ComponentModel.ISupportInitialize)(this.pbRoad1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRoad2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLitter1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLitter2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLitter3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLitter4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.track1R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.track1L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.track2L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.track2R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbtrashCan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hudsonSprite1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck1right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck1R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck1)).BeginInit();
            this.SuspendLayout();
            // 
            // pbRoad1
            // 
            this.pbRoad1.BackColor = System.Drawing.Color.DimGray;
            this.pbRoad1.Location = new System.Drawing.Point(0, 242);
            this.pbRoad1.Name = "pbRoad1";
            this.pbRoad1.Size = new System.Drawing.Size(669, 69);
            this.pbRoad1.TabIndex = 1;
            this.pbRoad1.TabStop = false;
            // 
            // pbRoad2
            // 
            this.pbRoad2.BackColor = System.Drawing.Color.DimGray;
            this.pbRoad2.Location = new System.Drawing.Point(0, 97);
            this.pbRoad2.Name = "pbRoad2";
            this.pbRoad2.Size = new System.Drawing.Size(669, 69);
            this.pbRoad2.TabIndex = 2;
            this.pbRoad2.TabStop = false;
            // 
            // gameTimer
            // 
            this.gameTimer.Interval = 50;
            this.gameTimer.Tick += new System.EventHandler(this.GameTimer_Tick);
            // 
            // lblLives
            // 
            this.lblLives.AutoSize = true;
            this.lblLives.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLives.Location = new System.Drawing.Point(12, 423);
            this.lblLives.Name = "lblLives";
            this.lblLives.Size = new System.Drawing.Size(66, 20);
            this.lblLives.TabIndex = 6;
            this.lblLives.Text = "Lives : 3";
            this.lblLives.Click += new System.EventHandler(this.LblLives_Click);
            // 
            // pbLitter1
            // 
            this.pbLitter1.BackColor = System.Drawing.Color.Teal;
            this.pbLitter1.Location = new System.Drawing.Point(81, 184);
            this.pbLitter1.Name = "pbLitter1";
            this.pbLitter1.Size = new System.Drawing.Size(42, 42);
            this.pbLitter1.TabIndex = 7;
            this.pbLitter1.TabStop = false;
            // 
            // pbLitter2
            // 
            this.pbLitter2.BackColor = System.Drawing.Color.Teal;
            this.pbLitter2.Location = new System.Drawing.Point(166, 340);
            this.pbLitter2.Name = "pbLitter2";
            this.pbLitter2.Size = new System.Drawing.Size(42, 42);
            this.pbLitter2.TabIndex = 8;
            this.pbLitter2.TabStop = false;
            // 
            // pbLitter3
            // 
            this.pbLitter3.BackColor = System.Drawing.Color.Teal;
            this.pbLitter3.Location = new System.Drawing.Point(392, 194);
            this.pbLitter3.Name = "pbLitter3";
            this.pbLitter3.Size = new System.Drawing.Size(42, 42);
            this.pbLitter3.TabIndex = 9;
            this.pbLitter3.TabStop = false;
            // 
            // pbLitter4
            // 
            this.pbLitter4.BackColor = System.Drawing.Color.Teal;
            this.pbLitter4.Location = new System.Drawing.Point(560, 184);
            this.pbLitter4.Name = "pbLitter4";
            this.pbLitter4.Size = new System.Drawing.Size(42, 42);
            this.pbLitter4.TabIndex = 10;
            this.pbLitter4.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Location = new System.Drawing.Point(660, 277);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1, 1);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // track1R
            // 
            this.track1R.BackColor = System.Drawing.Color.Transparent;
            this.track1R.Location = new System.Drawing.Point(618, 111);
            this.track1R.Name = "track1R";
            this.track1R.Size = new System.Drawing.Size(1, 1);
            this.track1R.TabIndex = 13;
            this.track1R.TabStop = false;
            // 
            // track1L
            // 
            this.track1L.Location = new System.Drawing.Point(-109, 111);
            this.track1L.Name = "track1L";
            this.track1L.Size = new System.Drawing.Size(1, 1);
            this.track1L.TabIndex = 14;
            this.track1L.TabStop = false;
            // 
            // track2L
            // 
            this.track2L.Location = new System.Drawing.Point(12, 259);
            this.track2L.Name = "track2L";
            this.track2L.Size = new System.Drawing.Size(1, 1);
            this.track2L.TabIndex = 15;
            this.track2L.TabStop = false;
            // 
            // track2R
            // 
            this.track2R.Location = new System.Drawing.Point(770, 259);
            this.track2R.Name = "track2R";
            this.track2R.Size = new System.Drawing.Size(1, 1);
            this.track2R.TabIndex = 16;
            this.track2R.TabStop = false;
            // 
            // lblInv
            // 
            this.lblInv.AutoSize = true;
            this.lblInv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInv.Location = new System.Drawing.Point(12, 403);
            this.lblInv.Name = "lblInv";
            this.lblInv.Size = new System.Drawing.Size(91, 20);
            this.lblInv.TabIndex = 19;
            this.lblInv.Text = "Inventory: 0";
            // 
            // pbtrashCan
            // 
            this.pbtrashCan.BackColor = System.Drawing.Color.Honeydew;
            this.pbtrashCan.inventory = 0;
            this.pbtrashCan.Location = new System.Drawing.Point(282, 12);
            this.pbtrashCan.Name = "pbtrashCan";
            this.pbtrashCan.Size = new System.Drawing.Size(71, 62);
            this.pbtrashCan.TabIndex = 18;
            this.pbtrashCan.TabStop = false;
            // 
            // truck45
            // 
            this.truck45.BackColor = System.Drawing.Color.Turquoise;
            this.truck45.CurrentDirection = Hudson.Direction.Right;
            this.truck45.Location = new System.Drawing.Point(146, 255);
            this.truck45.Name = "truck45";
            this.truck45.OffsetX = 0;
            this.truck45.Size = new System.Drawing.Size(109, 43);
            this.truck45.Speed = 0;
            this.truck45.TabIndex = 17;
            this.truck45.TabStop = false;
            // 
            // truck2
            // 
            this.truck2.BackColor = System.Drawing.Color.GhostWhite;
            this.truck2.CurrentDirection = Hudson.Direction.Left;
            this.truck2.Location = new System.Drawing.Point(177, 107);
            this.truck2.Name = "truck2";
            this.truck2.OffsetX = 0;
            this.truck2.Size = new System.Drawing.Size(109, 46);
            this.truck2.Speed = 0;
            this.truck2.TabIndex = 5;
            this.truck2.TabStop = false;
            // 
            // truck0
            // 
            this.truck0.BackColor = System.Drawing.Color.Black;
            this.truck0.CurrentDirection = Hudson.Direction.Left;
            this.truck0.Location = new System.Drawing.Point(448, 107);
            this.truck0.Name = "truck0";
            this.truck0.OffsetX = 0;
            this.truck0.Size = new System.Drawing.Size(109, 46);
            this.truck0.Speed = 0;
            this.truck0.TabIndex = 4;
            this.truck0.TabStop = false;
            // 
            // hudsonSprite1
            // 
            this.hudsonSprite1.BackColor = System.Drawing.SystemColors.Desktop;
            this.hudsonSprite1.FormHeight = 0;
            this.hudsonSprite1.FormWidth = 0;
            this.hudsonSprite1.Inventory = 0;
            this.hudsonSprite1.Lives = 0;
            this.hudsonSprite1.Location = new System.Drawing.Point(295, 364);
            this.hudsonSprite1.Name = "hudsonSprite1";
            this.hudsonSprite1.Size = new System.Drawing.Size(58, 59);
            this.hudsonSprite1.Speed = 0;
            this.hudsonSprite1.TabIndex = 0;
            this.hudsonSprite1.TabStop = false;
            // 
            // truck1right
            // 
            this.truck1right.BackColor = System.Drawing.Color.DarkOrchid;
            this.truck1right.Location = new System.Drawing.Point(519, 254);
            this.truck1right.Name = "truck1right";
            this.truck1right.OffsetX = 0;
            this.truck1right.Size = new System.Drawing.Size(136, 46);
            this.truck1right.Speed = 0;
            this.truck1right.TabIndex = 3;
            this.truck1right.TabStop = false;
            // 
            // truck1R
            // 
            this.truck1R.BackColor = System.Drawing.Color.DarkOrchid;
            this.truck1R.Location = new System.Drawing.Point(519, 254);
            this.truck1R.Name = "truck1R";
            this.truck1R.OffsetX = 0;
            this.truck1R.Size = new System.Drawing.Size(136, 46);
            this.truck1R.Speed = 0;
            this.truck1R.TabIndex = 3;
            this.truck1R.TabStop = false;
            // 
            // truck1
            // 
            this.truck1.BackColor = System.Drawing.Color.DarkOrchid;
            this.truck1.Location = new System.Drawing.Point(274, 190);
            this.truck1.Name = "truck1";
            this.truck1.OffsetX = 0;
            this.truck1.Size = new System.Drawing.Size(136, 46);
            this.truck1.Speed = 0;
            this.truck1.TabIndex = 4;
            this.truck1.TabStop = false;
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(625, 452);
            this.Controls.Add(this.lblInv);
            this.Controls.Add(this.pbtrashCan);
            this.Controls.Add(this.truck45);
            this.Controls.Add(this.track2R);
            this.Controls.Add(this.track2L);
            this.Controls.Add(this.track1L);
            this.Controls.Add(this.track1R);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pbLitter4);
            this.Controls.Add(this.pbLitter3);
            this.Controls.Add(this.pbLitter2);
            this.Controls.Add(this.pbLitter1);
            this.Controls.Add(this.lblLives);
            this.Controls.Add(this.truck2);
            this.Controls.Add(this.truck0);
            this.Controls.Add(this.pbRoad2);
            this.Controls.Add(this.pbRoad1);
            this.Controls.Add(this.hudsonSprite1);
            this.Name = "Game";
            this.Text = "Game";
            this.Load += new System.EventHandler(this.Game_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Game_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pbRoad1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRoad2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLitter1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLitter2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLitter3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLitter4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.track1R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.track1L)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.track2L)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.track2R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbtrashCan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hudsonSprite1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck1right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck1R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbRoad1;
        private System.Windows.Forms.PictureBox pbRoad2;
        private Hudson.HudsonSprite hudsonSprite1;
        private Hudson.Truck truck1right;
        private Hudson.Truck truck0;
        private Hudson.Truck truck2;
        private Hudson.Truck truck1R;
        private Hudson.Truck truck1;
        private System.Windows.Forms.Timer gameTimer;
        private System.Windows.Forms.Label lblLives;
        private System.Windows.Forms.PictureBox pbLitter1;
        private System.Windows.Forms.PictureBox pbLitter2;
        private System.Windows.Forms.PictureBox pbLitter3;
        private System.Windows.Forms.PictureBox pbLitter4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox track1R;
        private System.Windows.Forms.PictureBox track1L;
        private System.Windows.Forms.PictureBox track2L;
        private System.Windows.Forms.PictureBox track2R;
        private Truck truck45;
        private TrashCan pbtrashCan;
        private System.Windows.Forms.Label lblInv;
    }
}