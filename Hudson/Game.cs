using System;
using System.Windows.Forms;
using System.Drawing;

namespace Hudson
{

    public partial class Game : Form
    {
        int level = 1;
        public GameScore globalscore = new GameScore();
        private int id;
        
        public Game(int id)
        {
            
            InitializeComponent();
            this.id = id;
        }

        private void Game_Load(object sender, EventArgs e)
        {
            if (level == 1)
            {
                initHudson();
                initTrash(4);
                pbtrashCan.inventoryMax = 4;

                pbRoad1.SendToBack();
                pbRoad2.SendToBack();
                gameTimer.Enabled = true;
                gameTimer.Interval = 50;
                gameTimer.Start();

                truck0.setTrackLocation(track1L.Location, track1R.Location, track2L.Location, track2R.Location);
                truck2.setTrackLocation(track1L.Location, track1R.Location, track2L.Location, track2R.Location);
                truck45.setTrackLocation(track1L.Location, track1R.Location, track2L.Location, track2R.Location);

                truck0.Track = truck0.track1R;
                truck2.Track = truck2.track1R;
                truck45.Track = truck45.track2L;

                truck0.CurrentDirection = Direction.Left;
                truck2.CurrentDirection = Direction.Left;
                truck45.CurrentDirection = Direction.Right;

                truck0.Speed = 10;
                truck2.Speed = 10;
                truck45.Speed = 10;

                truck2.OffsetX = 200;

                truck0.Reset();
                truck2.Reset();
                truck45.Reset();
            }
            else if (level == 2)
            {
                initHudson();
                initTrash(4);
                pbtrashCan.inventoryMax = 8;

                pbRoad1.SendToBack();
                pbRoad2.SendToBack();
                gameTimer.Enabled = true;
                gameTimer.Interval = 50;
                gameTimer.Start();

                truck0.setTrackLocation(track1L.Location, track1R.Location, track2L.Location, track2R.Location);
                truck2.setTrackLocation(track1L.Location, track1R.Location, track2L.Location, track2R.Location);
                truck45.setTrackLocation(track1L.Location, track1R.Location, track2L.Location, track2R.Location);

                truck0.Track = truck0.track1R;
                truck2.Track = truck2.track1R;
                truck45.Track = truck45.track2L;

                truck0.CurrentDirection = Direction.Left;
                truck2.CurrentDirection = Direction.Left;
                truck45.CurrentDirection = Direction.Right;

                truck0.Speed = 15;
                truck2.Speed = 15;
                truck45.Speed = 15;

                truck2.OffsetX = 200;

                truck0.Reset();
                truck2.Reset();
                truck45.Reset();
            }
            else if (level == 3)
            {
                initHudson();
                initTrash(4);
                pbtrashCan.inventoryMax = 12;

                pbRoad1.SendToBack();
                pbRoad2.SendToBack();
                gameTimer.Enabled = true;
                gameTimer.Interval = 50;
                gameTimer.Start();

                truck0.setTrackLocation(track1L.Location, track1R.Location, track2L.Location, track2R.Location);
                truck2.setTrackLocation(track1L.Location, track1R.Location, track2L.Location, track2R.Location);
                truck45.setTrackLocation(track1L.Location, track1R.Location, track2L.Location, track2R.Location);

                truck0.Track = truck0.track1R;
                truck2.Track = truck2.track1R;
                truck45.Track = truck45.track2L;

                truck0.CurrentDirection = Direction.Left;
                truck2.CurrentDirection = Direction.Left;
                truck45.CurrentDirection = Direction.Right;

                truck0.Speed = 20;
                truck2.Speed = 20;
                truck45.Speed = 20;

                truck2.OffsetX = 200;

                truck0.Reset();
                truck2.Reset();
                truck45.Reset();
            }
        }

        private void initHudson()
        {
            hudsonSprite1.FormWidth = Width;
            hudsonSprite1.FormHeight = Height;
            hudsonSprite1.Speed = 5;
            hudsonSprite1.Lives = 3;
            hudsonSprite1.Inventory = 0;
        }
        
        private void Game_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.W:
                    hudsonSprite1.Move(Direction.Up);
                    break;
                case Keys.A:
                    hudsonSprite1.Move(Direction.Left);
                    break;
                case Keys.S:
                    hudsonSprite1.Move(Direction.Down);
                    break;
                case Keys.D:
                    hudsonSprite1.Move(Direction.Right);
                    break;
            }
        }

        private void initTrash(int trashNum)
        {
            Random rand = new Random();
            pbLitter1.Location = new Point(rand.Next(100, 550), rand.Next(10, 300));
            pbLitter2.Location = new Point(rand.Next(100, 550), rand.Next(10, 300));
            pbLitter3.Location = new Point(rand.Next(100, 550), rand.Next(10, 300));
            pbLitter4.Location = new Point(rand.Next(100, 550), rand.Next(10, 300));
        }

        
        private void GameTimer_Tick(object sender, EventArgs e)
        {
            int lives = hudsonSprite1.Lives;
            int inventory = hudsonSprite1.Inventory;
            lblInv.Text = ("Inventory: " + inventory);
            truck0.Drive();
            truck2.Drive();
            truck45.Drive();
            truck0.collisionAvoidance(track1L, track1R, track2L, track2R);
            truck2.collisionAvoidance(track1L, track1R, track2L, track2R);
            truck45.collisionAvoidance(track1L, track1R, track2L, track2R);
            
            if (hudsonSprite1.Bounds.IntersectsWith(truck0.Bounds) ||
                hudsonSprite1.Bounds.IntersectsWith(truck2.Bounds) ||
                hudsonSprite1.Bounds.IntersectsWith(truck45.Bounds))
            {
                hudsonSprite1.Lives -= 1;
                lives--;
                hudsonSprite1.Location = new Point(295, 364);
                lblLives.Text = ("Lives: " + lives);

                if (hudsonSprite1.Lives == 0)
                {
                    MessageBox.Show("You Lost!");
                    Close();
                }
            }

            if (hudsonSprite1.Bounds.IntersectsWith(pbLitter1.Bounds))
            {
                pbLitter1.Location = new Point(1000, 1000);
                hudsonSprite1.Inventory++;
                inventory++;

            }
            else if (hudsonSprite1.Bounds.IntersectsWith(pbLitter2.Bounds))
            {
                pbLitter2.Location = new Point(1000, 1000);
                hudsonSprite1.Inventory++;
                inventory++;
            }
            else if (hudsonSprite1.Bounds.IntersectsWith(pbLitter3.Bounds))
            {
                pbLitter3.Location = new Point(1000, 1000);
                hudsonSprite1.Inventory++;
                inventory++;
            }
            else if (hudsonSprite1.Bounds.IntersectsWith(pbLitter4.Bounds))
            {
                pbLitter4.Location = new Point(1000, 1000);
                hudsonSprite1.Inventory++;
                inventory++;
            }

            if (hudsonSprite1.Bounds.IntersectsWith(pbtrashCan.Bounds))
            {
                pbtrashCan.inventory += hudsonSprite1.Inventory;
                hudsonSprite1.Inventory = 0;
                if (pbtrashCan.inventory == pbtrashCan.inventoryMax && level == 1)
                {
                    level++;
                    gameTimer.Stop();
                    MathsQuiz mathswindow = new MathsQuiz(globalscore, id);
                    mathswindow.Show();
                    Hide();
                    mathswindow.Closed += (s, args) => Close();
                    
                    
                }
                else if (pbtrashCan.inventory == pbtrashCan.inventoryMax && level == 2)
                {
                    /* (GEOGRAPHY)
                     level++;
                    gameTimer.Stop();
                    MathsQuiz mathswindow = new MathsQuiz(score);
                    mathswindow.Show();
                    Hide();
                    quizwindow.Closed += (s, args) => Show();
                    quizwindow.Closed += Game_Load;
                    */
                }
                else if (pbtrashCan.inventory == pbtrashCan.inventoryMax && level == 3)
                {
                    /* (COMPUTER SCIENCE)
                    gameTimer.Stop();
                    Quiz quizwindow = new Quiz();
                    quizwindow.Show();
                    Hide();
                    quizwindow.Closed += (s, args) => Show();
                    quizwindow.Closed += Game_Load;
                    */
                }


            }
        }

        private void LblLives_Click(object sender, EventArgs e)
        {

        }
    }
}