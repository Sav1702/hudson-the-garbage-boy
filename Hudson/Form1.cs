﻿using System;
using System.Data.SQLite;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace Hudson
{
    public partial class Form1 : Form
    {
        private int id;
        public Form1()
        {
            this.id = id;
            InitializeComponent();
            lblWrongPword.Hide();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            var dbConnection = new SQLiteConnection("Data Source=Database.db;Version=3;");
            var db = new Database(dbConnection);

            var userExists = false;
            try
            {
                foreach (var user in db.users)
                    if (user.username == txtUname.Text)
                        userExists = true;
            }

            catch (SQLiteException exception)
            {
                Console.WriteLine(exception);
                db.ExecuteCommand(@"create table Users
(
    id       integer not null
        primary key,
    username text    not null,
    password text    not null
);

create table Scores
(
    id     integer
        constraint Scores_pk
            primary key,
    score  integer,
    date   text,
    userID integer
        references Users
);
                ");

            }

            if (userExists)
            {
                var sha256 = SHA256.Create();
                var passHash = Encoding.ASCII.GetString(sha256.ComputeHash(Encoding.ASCII.GetBytes(txtPword.Text)));

                var userQuery =
                    from user in db.users
                    where user.username == txtUname.Text && user.password == passHash
                    select user;

                if (userQuery.Any())
                {
                    Hide();
                    var ldg = new landing(userQuery.ToList().First().id.Value);
                    ldg.Show();
                    ldg.Closed += (s, args) => Show();
                }
                else
                {
                    lblWrongPword.Show();
                }
            }
            else
            {
                MessageBox.Show(
                    "New user. Please press login again with the same credentials and you will be registered");
                var sha256 = SHA256.Create();
                var passHash = Encoding.ASCII.GetString(sha256.ComputeHash(Encoding.ASCII.GetBytes(txtPword.Text)));
                var newUser = new user
                {
                    password = passHash,
                    username = txtUname.Text
                };

                db.users.InsertOnSubmit(newUser);
                try
                {
                    db.SubmitChanges();
                }
                catch (SQLiteException exception)
                {
                    db.SubmitChanges();
                }
            }
        }

        private void chbShowPass_CheckedChanged(object sender, EventArgs e)
        {
            txtPword.UseSystemPasswordChar = !txtPword.UseSystemPasswordChar;
        }
    }
}