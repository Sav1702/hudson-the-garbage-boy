﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hudson
{
    public partial class Scores : Form
    {
        private int id;
        public Scores(int id)
        {
            this.id = id;
            InitializeComponent();
        }

        private void Scores_Load(object sender, EventArgs e)
        {
            var dbConnection = new SQLiteConnection("Data Source=Database.db;Version=3;");
            var db = new Database(dbConnection);

            var scoreQuery =
                from score in db.scores
                where score.userID == id
                select score;

            ScoreGrid.DataSource = scoreQuery;

            ScoreGrid.Columns.Remove("id");
            ScoreGrid.Columns.Remove("userID");
            ScoreGrid.Columns[0].HeaderText = "Date";
            ScoreGrid.Columns[1].HeaderText = "Score";

        }
    }
}
