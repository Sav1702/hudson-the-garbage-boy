﻿namespace Hudson
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpDetails = new System.Windows.Forms.GroupBox();
            this.lblWrongPword = new System.Windows.Forms.Label();
            this.chbShowPass = new System.Windows.Forms.CheckBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtPword = new System.Windows.Forms.TextBox();
            this.txtUname = new System.Windows.Forms.TextBox();
            this.lblPword = new System.Windows.Forms.Label();
            this.lblUname = new System.Windows.Forms.Label();
            this.grpDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDetails
            // 
            this.grpDetails.Controls.Add(this.lblWrongPword);
            this.grpDetails.Controls.Add(this.chbShowPass);
            this.grpDetails.Controls.Add(this.btnLogin);
            this.grpDetails.Controls.Add(this.txtPword);
            this.grpDetails.Controls.Add(this.txtUname);
            this.grpDetails.Controls.Add(this.lblPword);
            this.grpDetails.Controls.Add(this.lblUname);
            this.grpDetails.Location = new System.Drawing.Point(19, 155);
            this.grpDetails.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.grpDetails.Name = "grpDetails";
            this.grpDetails.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.grpDetails.Size = new System.Drawing.Size(271, 224);
            this.grpDetails.TabIndex = 0;
            this.grpDetails.TabStop = false;
            this.grpDetails.Text = "User Details";
            // 
            // lblWrongPword
            // 
            this.lblWrongPword.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic);
            this.lblWrongPword.ForeColor = System.Drawing.Color.Red;
            this.lblWrongPword.Location = new System.Drawing.Point(78, 126);
            this.lblWrongPword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblWrongPword.Name = "lblWrongPword";
            this.lblWrongPword.Size = new System.Drawing.Size(141, 17);
            this.lblWrongPword.TabIndex = 6;
            this.lblWrongPword.Text = "Incorrect password";
            // 
            // chbShowPass
            // 
            this.chbShowPass.Location = new System.Drawing.Point(78, 144);
            this.chbShowPass.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chbShowPass.Name = "chbShowPass";
            this.chbShowPass.Size = new System.Drawing.Size(144, 25);
            this.chbShowPass.TabIndex = 5;
            this.chbShowPass.Text = "Show Password";
            this.chbShowPass.UseVisualStyleBackColor = true;
            this.chbShowPass.CheckedChanged += new System.EventHandler(this.chbShowPass_CheckedChanged);
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(51, 175);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(170, 28);
            this.btnLogin.TabIndex = 4;
            this.btnLogin.Text = "Log In";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txtPword
            // 
            this.txtPword.Location = new System.Drawing.Point(78, 100);
            this.txtPword.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtPword.Name = "txtPword";
            this.txtPword.Size = new System.Drawing.Size(185, 23);
            this.txtPword.TabIndex = 3;
            this.txtPword.UseSystemPasswordChar = true;
            // 
            // txtUname
            // 
            this.txtUname.Location = new System.Drawing.Point(78, 54);
            this.txtUname.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtUname.Name = "txtUname";
            this.txtUname.Size = new System.Drawing.Size(185, 23);
            this.txtUname.TabIndex = 2;
            // 
            // lblPword
            // 
            this.lblPword.AutoSize = true;
            this.lblPword.Location = new System.Drawing.Point(7, 103);
            this.lblPword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPword.Name = "lblPword";
            this.lblPword.Size = new System.Drawing.Size(57, 15);
            this.lblPword.TabIndex = 1;
            this.lblPword.Text = "Password";
            // 
            // lblUname
            // 
            this.lblUname.AutoSize = true;
            this.lblUname.Location = new System.Drawing.Point(7, 58);
            this.lblUname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUname.Name = "lblUname";
            this.lblUname.Size = new System.Drawing.Size(60, 15);
            this.lblUname.TabIndex = 0;
            this.lblUname.Text = "Username";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 407);
            this.Controls.Add(this.grpDetails);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Form1";
            this.grpDetails.ResumeLayout(false);
            this.grpDetails.PerformLayout();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.GroupBox grpDetails;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtPword;
        private System.Windows.Forms.TextBox txtUname;
        private System.Windows.Forms.Label lblPword;
        private System.Windows.Forms.CheckBox chbShowPass;
        private System.Windows.Forms.Label lblWrongPword;
        private System.Windows.Forms.Label lblUname;
    }
}

